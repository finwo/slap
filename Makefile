CC?=clang
CFLAGS?=-Os -Wall -fdata-sections -ffunction-sections
INCLUDES?=
LIBS?=

SRC=$(wildcard src/*.c)

INCLUDES+=-I lib/finwo/http-parser/src
SRC+=lib/finwo/http-parser/src/http-parser.c

INCLUDES+=-I lib/jacketizer/libyuarel
SRC+=lib/jacketizer/libyuarel/yuarel.c

INCLUDES+=-I lib/antirez/linenoise
SRC+=lib/antirez/linenoise/linenoise.c

INCLUDES+=-I lib/cofyc/argparse
SRC+=lib/cofyc/argparse/argparse.c

OBJ=$(SRC:.c=.o)

.c.o:
	$(CC) $(INCLUDES) $(LIBS) $(CFLAGS) -c -o $@ $^

slap: $(OBJ)
	$(CC) $(INCLUDES) $(LIBS) $(CFLAGS) -s -o $@ $^ -Wl,--gc-sections

.PHONY: clean
clean:
	rm -f $(OBJ)
