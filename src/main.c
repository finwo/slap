#ifdef __cplusplus
extern "C" {
#endif

int main() {
  return 42;
}

#ifdef __cplusplus
} // extern "C"
#endif
