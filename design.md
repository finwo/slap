/opt/<provider>/<package>/.slap
    [source]
    keep=true/false

    [repository]
    git=https://github.com/finwo/slap.git
    branch=master
    ref=34b95de1d166b67b79187874d41ec0b9178b7bc5

    [repository]
    tarball=https://github.com/finwo/slap/archive/master.tar.gz
    etag=W/"5efb489cdbda94f5ac44c494de8010633c5c1b46e8d4660873447c085ee49682"

/opt/<provider>/<package>/src/
    git repo or tarball contents
    pulled and/or overwritten

/opt/bin
    symlinks into /opt/<provider>/<package>/bin/
